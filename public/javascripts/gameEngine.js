// primary module game 
angular.module('gameApp')

.factory("gameEngine",["$rootScope" ,"gameConstants","playerService","enemyService", function($rootScope,gameConstants, playerService,enemyService){
	

	return {
		// inicialize display canvas , player position and enemies positions 
		init: function(){
			$rootScope.playerLives = 3;
			Crafty.init(gameConstants.initGame.widthScreen,gameConstants.initGame.heightScreen);
			playerService.playerInit(gameConstants.positionPlayer.startX,gameConstants.positionPlayer.startY);
			playerService.drawPlayer(gameConstants.positionPlayer.startX,gameConstants.positionPlayer.startY);
			playerService.drawPlayerLives();
			enemyService.enemiesInit(gameConstants.positionEnemy.startX,gameConstants.positionEnemy.startY);
			enemyService.drawEnemies();

			$rootScope.fire = [5];
			for (var i = 0; i <=5; i++) {
				$rootScope.fire[i] = new Object();
			};
		},

		loopGame : function(){

			if($rootScope.event){
				//console.log($rootScope.event.keyCode);
				$rootScope.strzal = Crafty("Fire").length;
				switch($rootScope.event.keyCode){
					case 32 : 
								if($rootScope.strzal<5){
									playerService.fire($rootScope.fireQuality);
								}
							break;
					case 37 : 
							playerService.updatePlayer(-10,0,2);
							break;
					case 38 : 
							playerService.updatePlayer(0,-10,1);
							break;
					case 39 : 
							playerService.updatePlayer(10,0,3);
							break;
					case 40 : 
							playerService.updatePlayer(0,10,4);
							break;
				}
			}else{
				$rootScope.event = null;
			}

			if($rootScope.fire[0].move){
				playerService.firePositionChange(0);
				$rootScope.fire[0].move--;
			}else{
				$rootScope.fireQuality = 0;
			}

			if($rootScope.fire[1].move){
				playerService.firePositionChange(1);
				$rootScope.fire[1].move--;
			}else{
				$rootScope.fireQuality = 1;
			}

			if($rootScope.fire[2].move){
				playerService.firePositionChange(2);
				$rootScope.fire[2].move--;
			}else{
				$rootScope.fireQuality = 2;
			}

			if($rootScope.fire[3].move){
				playerService.firePositionChange(3);
				$rootScope.fire[3].move--;
			}else{
				$rootScope.fireQuality = 3;
			}

			if($rootScope.fire[4].move){
				playerService.firePositionChange(4);
				$rootScope.fire[4].move--;
			}else{
				$rootScope.fireQuality = 4;
			}



		},

		// stop dispay canvas 
		exit: function(){
			Crafty.stop(1);
		}
	}
}])
