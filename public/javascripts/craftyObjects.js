    Crafty.sprite(36,'../images/space.png',{
        space_player_up: [0,0],
        space_player_up_right: [1,0],
        space_player_up_left: [2,0],
        space_player_down: [3,0],
        space_player_down_right: [4,0],
        space_player_down_left: [5,0]

    });
    Crafty.sprite(15,'../images/sprite.png',{
        live: [0,0],
        fire: [1,0]
    })
    Crafty.sprite(36,'../images/enemies.png',{
        space1: [0,0],
        space2: [1,0],
        space3: [1,1],
        space4: [0,1],
        space5: [0,0],
        space6: [0,0],
        space7: [0,0]
    });

    Crafty.c('Player', {
        init: function () {
            this
                .addComponent("2D,DOM,canvas,space_player_up")
        },

        destroyMe: function(){
            this.destroy();
        }
    })

    Crafty.c('Live', {
        init: function(){
            this
            .addComponent("2D,DOM,canvas,Collision,live")
            
        }
    })

    Crafty.c('Fire', {
        init: function(){
            this
            .addComponent("2D,DOM,canvas,Collision,fire")
            .onHit('Enemy1',this.destroyEnemy,this.destroy)
            .onHit('Enemy2',this.destroyEnemy,this.destroy)
            .onHit('Enemy3',this.destroyEnemy,this.destroy)
            .onHit('Enemy4',this.destroyEnemy,this.destroy)
            .onHit('Enemy5',this.destroyEnemy,this.destroy)
            .onHit('Wall',this.destroy);
        },

        destroyEnemy: function(data){
            enemy = data[0].obj;
            enemy.destroyMe();
        }
    })

    Crafty.c('Wall', {
        init: function(){
            this
            .addComponent("2D,DOM,canvas")
            .attr({x:0,y:50,w:640})
        }
    })


	Crafty.c('Enemy1', {
    	init: function () {
            this
            .addComponent("2D,DOM,canvas,Collision,space1")
            .onHit('Player', this.destroyPlayer)
    	},

    	destroyPlayer: function(data){
    		player = data[0].obj;
    		player.destroyMe();
    	},

        destroyMe: function(){
            this.destroy();
        }
	})
    Crafty.c('Enemy2', {
        init: function () {
            this
            .addComponent("2D,DOM,canvas,Collision,space2")
            .onHit('Player', this.destroyPlayer)
        },

        destroyPlayer: function(data){
            player = data[0].obj;
            player.destroyMe();
        },

        destroyMe: function(){
            this.destroy();
        }
    })
    Crafty.c('Enemy3', {
        init: function () {
            this
            .addComponent("2D,DOM,canvas,Collision,space3")
            .onHit('Player', this.destroyPlayer)
        },

        destroyPlayer: function(data){
            player = data[0].obj;
            player.destroyMe();
        },

            destroyMe: function(){
            this.destroy();
        }
    })
    Crafty.c('Enemy4', {
        init: function () {
            this
            .addComponent("2D,DOM,canvas,Collision,space4")
            .onHit('Player', this.destroyPlayer)
        },

        destroyPlayer: function(data){
            player = data[0].obj;
            player.destroyMe();
        },

        destroyMe: function(){
            this.destroy();
        }
    })
    Crafty.c('Enemy5', {
        init: function () {
            this
            .addComponent("2D,DOM,canvas,Collision,space5")
            .onHit('Player', this.destroyPlayer)
        },

        destroyPlayer: function(data){
            player = data[0].obj;
            player.destroyMe();
        },

        destroyMe: function(){
            this.destroy();
        }
    })
