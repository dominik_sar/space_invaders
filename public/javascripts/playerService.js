angular.module("gameApp")

.factory("playerService",["gameConstants","$rootScope", function(gameConstants,$rootScope){

	return {

		playerInit : function(x,y) {
			$rootScope.positionPlayerX = x;
			$rootScope.positionPlayerY = y;
			Crafty.e('Wall');
		},

		drawPlayer : function(x,y) {
			$rootScope.player = Crafty.e("Player")
			.attr({x:x, y:y})
		},

		drawPlayerLives : function(){
			Crafty.e("Live").attr({x:10,y:10});
			Crafty.e("Live").attr({x:25,y:10});
			Crafty.e("Live").attr({x:40,y:10});
		},

		updatePlayer : function(x,y,direction){

			var tX = $rootScope.positionPlayerX + x, tY = $rootScope.positionPlayerY + y;
			if(this.checkPosition(tX,tY)){
				$rootScope.positionPlayerX += x;
				$rootScope.positionPlayerY += y;
				switch(direction){ 
					case 1 : 	
										$rootScope.spritePlayerUp = true;
										$rootScope.player.removeComponent($rootScope.spritePlayer);
										$rootScope.spritePlayer = 'space_player_up';
										$rootScope.player.addComponent($rootScope.spritePlayer);
										break;
					case 2 : 
										$rootScope.player.removeComponent($rootScope.spritePlayer);
										if($rootScope.spritePlayerUp===false){
											$rootScope.spritePlayer = 'space_player_down_left';
											$rootScope.player.addComponent($rootScope.spritePlayer);
										}else{
											$rootScope.spritePlayer = 'space_player_up_left';
											$rootScope.player.addComponent($rootScope.spritePlayer);
										}
										
										break;
					case 3 :  
										if($rootScope.spritePlayerUp==false){
											$rootScope.player.removeComponent($rootScope.spritePlayer);
											$rootScope.spritePlayer = 'space_player_down_right';
										}else{
											$rootScope.player.removeComponent($rootScope.spritePlayer);
											$rootScope.spritePlayer = 'space_player_up_right';
										}
										$rootScope.player.addComponent($rootScope.spritePlayer);
										break;
					case 4 : 	
										$rootScope.spritePlayerUp = false;
										$rootScope.player.removeComponent($rootScope.spritePlayer);
										$rootScope.spritePlayer = 'space_player_down';
										$rootScope.player.addComponent($rootScope.spritePlayer);
										break;
				}
				$rootScope.player.attr({x:$rootScope.positionPlayerX, y:$rootScope.positionPlayerY});
			}else{
				$rootScope.player.removeComponent($rootScope.spritePlayer);
				$rootScope.spritePlayer = 'space_player_up';
				$rootScope.player.attr({x:$rootScope.positionPlayerX, y:$rootScope.positionPlayerY});
			}
			delete tX;
			delete tY;
			
		},

		checkPosition : function(x,y){

			if(x>=0 && x<=640-36 && y>=0 && y<=480-36){
				return true;
			}else{
				return false;
			}
		},

		fire: function(i){
			$rootScope.fire[i].pozX = $rootScope.positionPlayerX+10;
			$rootScope.fire[i].pozY = $rootScope.positionPlayerY;
			$rootScope.fire[i].element = Crafty.e('Fire')
			.attr({x:$rootScope.fire[i].pozX, y:$rootScope.fire[i].pozY});
			$rootScope.fire[i].move = 40;
			console.log(Crafty("Fire").length)
		},

		firePositionChange: function(i){
			$rootScope.fire[i].pozY-=15;
			$rootScope.fire[i].element.attr({x:$rootScope.fire[i].pozX, y:$rootScope.fire[i].pozY})
		}
	}


}]);