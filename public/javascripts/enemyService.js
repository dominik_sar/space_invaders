angular.module("gameApp")

.factory("enemyService",["gameConstants","$rootScope", function(gameConstants,$rootScope){

	return {
		
		enemiesInit : function(x,y){
				$rootScope.positionEnemyX = x;
				$rootScope.positionEnemyY = y;
		},

		drawEnemies : function() {

			
			var XX = $rootScope.positionEnemyX;
			var YY = $rootScope.positionEnemyY;
			console.log($rootScope.positionEnemyX+ "   ---  "+$rootScope.positionEnemyY);
			var space = 1;
			for(var i=0; i<5; i++){
				for(var j=0; j<10; j++){
					switch(space){
						case 1:
							Crafty.e("Enemy1")
							.attr({x:XX, y:YY});
							break;
						case 2:
							Crafty.e("Enemy2")
							.attr({x:XX, y:YY});
							break;
						case 3:
							Crafty.e("Enemy3")
							.attr({x:XX, y:YY});
							break;
						case 4:
							Crafty.e("Enemy4")
							.attr({x:XX, y:YY});
							break;
					}
					XX+=50;
				}
				space++;
				YY=YY+50;
				XX=$rootScope.positionEnemyX;
				
			}

		},

		updateEnemies : function(x,y){
			this.setPositionEnemies(x,y);

		}
	}


}]);