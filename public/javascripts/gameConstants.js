// constants of game 
angular.module('gameApp')
.constant("gameConstants", {
	
	initGame : {
		widthScreen : 640,
		heightScreen : 480,
		sizeSprite : 36,
	},

	positionPlayer : { 
		startX : 640/2 - 18,
		startY : 480 - 36
	},

	positionEnemy : {
		startX : 10,
		startY : 50
	},

	gameState : {
		gameActive: 0,
		killedPlayer: 1,
		newLevel: 2,
		gameOver: 3
	},

	pointsInvaders : {
		space1 : 10,
		space2 : 20,
		space3 : 30,
		space4 : 40,
		space5 : 50,
		space6 : 100,
		space7 : 200
	}


});