var app = angular.module('gameApp',['ngRoute']).run(function($rootScope){

	$rootScope.positionPlayerX;
	$rootScope.positionPlayerY;
	$rootScope.spritePlayer = "space_player";
	$rootScope.spritePlayerUp = true;
	$rootScope.player;

	$rootScope.firePositionX;
	$rootScope.firePositionY;
	$rootScope.moveFire;
	$rootScope.fireQuality=0;
	$rootScope.fire;


	$rootScope.positionEnemyX;
	$rootScope.positionEnemyY;



});
app.config(function($routeProvider){
	$routeProvider
	.when('/', {
		templateUrl: 'views/main.html',
		controller: 'mainController'
	})

})
app.controller('mainController',['$scope', function($scope){

	$scope.playGame = function(){
		if(!$scope.clickPlay){
			$scope.clickPlay = true;
			return;
		}
	}

	$scope.showHighscores = function(){
		if(!$scope.clickHighscores){
			$scope.clickHighscores = true;
			return;
		}
	}

	$scope.returnMenu = function(){
		$scope.clickPlay = false;
		$scope.clickHighscores = false;
		return;
	}




}])

app.directive('spaceInvadersGame',["$interval","gameEngine", function($interval,gameEngine) {
   return {
    templateUrl: 'views/play.html',
    link: function(){
    	gameEngine.init();

    	$interval(gameEngine.loopGame,50);

    }
  };
}]);


